import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import VueTheMask from 'vue-the-mask'
import money from 'v-money'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/stylesheets/custom.scss'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueTheMask)
Vue.use(money, {
  decimal: ',',
  thousands: '.',
  prefix: 'R$ ',
  precision: 2,
  masked: false
})

new Vue({
  render: h => h(App)
}).$mount('#app')
